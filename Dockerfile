# visimo/coeus-proxy:latest
FROM nginx:1.19.10

ENV DOMAIN_BACK   'backend'
ENV DOMAIN_FRONT  'frontend'

# copy in configuration
COPY nginx.conf  /etc/nginx/nginx.conf
COPY templates   /etc/nginx/templates
